package usecase

import (
	"errors"

	"bitbucket.org/Amartha/go-single-ddd/modules/health/model"
	"bitbucket.org/Amartha/go-single-ddd/modules/health/query"
)

// HealthUseCaseImpl - structure for health use case implementation
type HealthUseCaseImpl struct {
	healthQuery query.HealthQuery
}

// NewHealthUseCase - function for initializing health use case implementation
func NewHealthUseCase(healthQuery query.HealthQuery) HealthUseCase {
	return &HealthUseCaseImpl{
		healthQuery: healthQuery,
	}
}

// Ping function for checking service
func (hu *HealthUseCaseImpl) Ping(message string) <-chan ResultUseCase {
	output := make(chan ResultUseCase)

	go func() {
		defer close(output)

		healthResult := <-hu.healthQuery.Ping(message)

		if healthResult.Error != nil {
			output <- ResultUseCase{Error: healthResult.Error}
			return
		}

		health, ok := healthResult.Result.(model.Health)

		if !ok {
			output <- ResultUseCase{Error: errors.New("result is not health")}
			return
		}

		output <- ResultUseCase{Result: health}

	}()

	return output
}
