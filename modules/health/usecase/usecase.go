package usecase

// ResultUseCase - structure for use case result
type ResultUseCase struct {
	Result interface{}
	Error  error
}

// HealthUseCase - interface for health use case
type HealthUseCase interface {
	Ping(message string) <-chan ResultUseCase
}
