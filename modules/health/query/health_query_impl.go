package query

import (
	"math/rand"

	"bitbucket.org/Amartha/go-single-ddd/modules/health/model"
)

// HealthQueryImpl - structure for health query implementation
type HealthQueryImpl struct {
	name string
}

// NewHealthQueryImpl - function for initializing health query implementation
func NewHealthQueryImpl(name string) HealthQuery {
	return &HealthQueryImpl{name}
}

// Ping - function for checking service
func (q *HealthQueryImpl) Ping(message string) <-chan ResultQuery {

	output := make(chan ResultQuery)

	go func() {
		defer close(output)

		rd := rand.Intn(100)

		var health model.Health
		health.Message = q.name
		health.Query = message
		health.Rand = rd

		output <- ResultQuery{Result: health}

	}()

	return output
}
