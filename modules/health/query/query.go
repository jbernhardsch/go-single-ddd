package query

// ResultQuery - structure for query result
type ResultQuery struct {
	Result interface{}
	Error  error
}

// HealthQuery - interface abstraction for health query
type HealthQuery interface {
	Ping(message string) <-chan ResultQuery
}
