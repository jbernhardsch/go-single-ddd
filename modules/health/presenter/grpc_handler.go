package presenter

import (
	"errors"
	"fmt"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/Amartha/go-single-ddd/modules/health/model"
	"bitbucket.org/Amartha/go-single-ddd/modules/health/usecase"
	pb "bitbucket.org/Amartha/go-single-ddd/protogo/health"
)

// GRPCHandler data structure
type GRPCHandler struct {
	healthUseCase usecase.HealthUseCase
}

// NewGRPCHandler function for initializing grpc handler object
func NewGRPCHandler(healthUseCase usecase.HealthUseCase) *GRPCHandler {
	return &GRPCHandler{healthUseCase}
}

// PingPong function for implementing function from health protobuf
func (h *GRPCHandler) PingPong(c context.Context, arg *pb.PingRequest) (*pb.PongResponse, error) {
	ctx := "HealthPresenter-PingPong"

	message := arg.Ping

	if len(message) == 0 {
		err := errors.New("message is required")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "ping_pong")
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	result := <-h.healthUseCase.Ping(message)

	if result.Error != nil {
		helper.Log(log.ErrorLevel, result.Error.Error(), ctx, "ping_pong")
		return nil, status.Error(codes.Internal, result.Error.Error())
	}

	pong, ok := result.Result.(model.Health)

	if !ok {
		err := errors.New("result is not health")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "ping_pong")
		return nil, status.Error(codes.Internal, err.Error())
	}

	msg := fmt.Sprintf("Pong * %s", pong.Message)

	pongRes := &pb.PongResponse{
		Pong: msg,
		Rand: int32(pong.Rand),
	}

	return pongRes, nil
}
