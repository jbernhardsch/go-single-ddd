package presenter

import (
	"errors"
	"net/http"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	"bitbucket.org/Amartha/go-single-ddd/modules/health/model"
	"bitbucket.org/Amartha/go-single-ddd/modules/health/usecase"
	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

// HTTPHealthHandler - structure for health http handler
type HTTPHealthHandler struct {
	healthUseCase usecase.HealthUseCase
}

// NewHTTPHandler - for initializing http health model
func NewHTTPHandler(healthUseCase usecase.HealthUseCase) *HTTPHealthHandler {
	return &HTTPHealthHandler{healthUseCase: healthUseCase}
}

// Mount - function for mounting routes
func (h *HTTPHealthHandler) Mount(group *echo.Group) {
	group.GET("", h.Ping)
}

// Ping - function for checking service
func (h *HTTPHealthHandler) Ping(c echo.Context) error {
	ctx := "HealthPresenter-Ping"

	pingQuery := c.QueryParam("ping")

	if len(pingQuery) <= 0 {
		err := errors.New("ping query is required")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "ping_query")
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	pingResult := <-h.healthUseCase.Ping(pingQuery)

	if pingResult.Error != nil {
		err := errors.New("cannot ping server")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "ping_server")
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	ping, ok := pingResult.Result.(model.Health)
	if !ok {
		err := errors.New("result is not ping")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "assert_ping_interface")
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, ping)

}
