package model

// Health data structure
type Health struct {
	Message string `json:"message"`
	Query   string `json:"query"`
	Rand    int    `json:"rand"`
}
