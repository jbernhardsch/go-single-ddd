package query

// ResultQuery - structure for query result
type ResultQuery struct {
	Result interface{}
	Error  error
}

type UserQuery interface {
	FindAllUsers() <-chan ResultQuery
}

type RedisUserQuery interface {
	Find() <-chan ResultQuery
}
