package query

import (
	"testing"
	"time"

	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
	sqlMock "gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/stretchr/testify/assert"
)

func TestUserQueryPostgres(t *testing.T) {
	t.Run("Test Find All User", func(t *testing.T) {
		db, mock, _ := sqlMock.New()
		defer db.Close()

		rows := sqlMock.NewRows(
			[]string{"id", "name", "created", "updated",
			}).
			AddRow(1, "Julius Bernhard", time.Now(), time.Now()).
			AddRow(2, "Julius Bernhard Schluter", time.Now(), time.Now())

		query := `SELECT id, name, created, updated FROM users`

		mock.ExpectPrepare(query).ExpectQuery().WillReturnRows(rows)

		us := NewUserQueryPostgres(db)

		userResult := <-us.FindAllUsers()

		assert.NoError(t, userResult.Error)
		assert.IsType(t, []model.User{}, userResult.Result)
	})
}