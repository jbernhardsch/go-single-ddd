package query

import (
	"database/sql"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
	log "github.com/Sirupsen/logrus"
	"github.com/lib/pq"
)

// UserQueryPostgres - structure for user query
type UserQueryPostgres struct {
	db *sql.DB
}

// NewUserQueryPostgres - function for initializing user query
func NewUserQueryPostgres(db *sql.DB) *UserQueryPostgres {
	return &UserQueryPostgres{db: db}
}

func (uq *UserQueryPostgres) FindAllUsers() <-chan ResultQuery {
	ctx := "UserQuery-FindAllUsers"

	output := make(chan ResultQuery)
	go func() {
		defer close(output)

		sq := `SELECT id, name, created, updated FROM users`

		stmt, err := uq.db.Prepare(sq)
		if err != nil {
			helper.Log(log.ErrorLevel, err.Error(), ctx, "prepare_database")
			output <- ResultQuery{Error: err}
			return
		}

		defer stmt.Close()

		rows, err := stmt.Query()
		if err != nil {
			helper.Log(log.ErrorLevel, err.Error(), ctx, "query_database")
			output <- ResultQuery{Error: nil}
			return
		}

		defer rows.Close()

		var users []model.User
		for rows.Next() {
			var (
				user    model.User
				updated pq.NullTime
			)

			err = rows.Scan(
				&user.ID, &user.Name, &user.Created, &updated,
			)

			if err != nil {
				helper.Log(log.ErrorLevel, err.Error(), ctx, "query_database")
				output <- ResultQuery{Error: err}
				return
			}

			if updated.Valid {
				user.Updated = updated.Time
			}

			users = append(users, user)
		}

		output <- ResultQuery{Result: users}
	}()

	return output
}
