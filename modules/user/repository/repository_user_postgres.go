package repository

import (
	"database/sql"
	"github.com/lib/pq"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
	log "github.com/Sirupsen/logrus"
)

// UserRepoPostgres - data structure for user repository
type UserRepoPostgres struct {
	db *sql.DB
}

// NewUserRepoPostgres - function for initializing user repository
func NewUserRepoPostgres(db *sql.DB) *UserRepoPostgres {
	return &UserRepoPostgres{db: db}
}

// Save - function for saving member data
func (ur *UserRepoPostgres) Save(user model.User) <-chan ResultRepository {
	ctx := "UserRepository-Save"

	output := make(chan ResultRepository)
	go func() {
		defer close(output)

		tx, err := ur.db.Begin()

		sq := `INSERT INTO users (name) VALUES ($1)`

		stmt, err := tx.Prepare(sq)
		defer stmt.Close()

		if err != nil {
			tx.Rollback()
			helper.Log(log.ErrorLevel, err.Error(), ctx, "insert_user")
			output <- ResultRepository{Error: err}
			return
		}

		_, err = stmt.Exec(user.Name)

		if err != nil {
			tx.Rollback()
			helper.Log(log.ErrorLevel, err.Error(), ctx, "execute_query_statement")
			output <- ResultRepository{Error: err}
			return
		}

		// commit statement
		tx.Commit()

		output <- ResultRepository{Error: nil}

	}()

	return output
}

// Load - function for loading single detail data
func (ur *UserRepoPostgres) Load(id int) <-chan ResultRepository {
	ctx := "UserRepository-Load"

	output := make(chan ResultRepository)
	go func() {
		defer close(output)

		sq := `SELECT id, name, created, updated FROM users WHERE id = $1`

		stmt, err := ur.db.Prepare(sq)
		defer stmt.Close()

		if err != nil {
			helper.Log(log.ErrorLevel, err.Error(), ctx, "prepare_database")
			output <- ResultRepository{Error: err}
			return
		}

		var (
			user    model.User
			updated pq.NullTime
		)

		err = stmt.QueryRow(id).Scan(
			&user.ID, &user.Name, &user.Created, &updated,
		)

		if err != nil {
			helper.Log(log.ErrorLevel, err.Error(), ctx, "query_database")
			output <- ResultRepository{Error: err}
			return
		}

		if updated.Valid {
			user.Updated = updated.Time
		}

		output <- ResultRepository{Result: user}
	}()

	return output
}
