package repository

import (
	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
)

// ResultRepository data structure
type ResultRepository struct {
	Result interface{}
	Error  error
}

// UserRepository - interface abstraction for user
type UserRepository interface {
	Save(user model.User) <-chan ResultRepository
	Load(id int) <-chan ResultRepository
}
