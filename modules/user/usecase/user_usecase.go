package usecase

import (
	"errors"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/query"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/repository"
	log "github.com/Sirupsen/logrus"
)

// UserUseCaseImpl - structure for user use case implementation
type UserUseCaseImpl struct {
	userQuery query.UserQuery
	userRepo  repository.UserRepository
}

// NewUserUseCase - function for initializing user use case implementation
func NewUserUseCase(userQuery query.UserQuery, userRepo repository.UserRepository) UserUseCase {
	return &UserUseCaseImpl{userQuery: userQuery, userRepo: userRepo}
}

// GetUsers - function for getting list of users
func (uq *UserUseCaseImpl) GetUsers() <-chan ResultUseCase {
	ctx := "UserUseCase-GetUsers"

	output := make(chan ResultUseCase)
	go func() {
		defer close(output)

		usersResult := <-uq.userQuery.FindAllUsers()
		if usersResult.Error != nil {
			helper.Log(log.ErrorLevel, usersResult.Error.Error(), ctx, "get_users")
			output <- ResultUseCase{Error: usersResult.Error}
			return
		}

		users, ok := usersResult.Result.([]model.User)
		if !ok {
			err := errors.New("result is not list of user")
			helper.Log(log.ErrorLevel, err.Error(), ctx, "assert_users")
			output <- ResultUseCase{Error: err}
			return
		}

		output <- ResultUseCase{Result: users}

	}()

	return output
}

// GetUserByID - function for getting detail user by id
func (uq *UserUseCaseImpl) GetUserByID(id int) <-chan ResultUseCase {
	ctx := "UserUseCase-GetUsers"

	output := make(chan ResultUseCase)
	go func() {
		defer close(output)

		userResult := <-uq.userRepo.Load(id)
		if userResult.Error != nil {
			helper.Log(log.ErrorLevel, userResult.Error.Error(), ctx, "get_user")
			output <- ResultUseCase{Error: userResult.Error}
			return
		}

		user, ok := userResult.Result.(model.User)
		if !ok {
			err := errors.New("result is not user")
			helper.Log(log.ErrorLevel, err.Error(), ctx, "assert_user")
			output <- ResultUseCase{Error: err}
			return
		}

		output <- ResultUseCase{Result: user}

	}()

	return output
}

func (uq *UserUseCaseImpl) SaveUser(user model.User) <-chan ResultUseCase {
	ctx := "UserUseCase-SaveUser"

	output := make(chan ResultUseCase)
	go func() {
		defer close(output)

		saveResult := <-uq.userRepo.Save(user)
		if saveResult.Error != nil {
			helper.Log(log.ErrorLevel, saveResult.Error.Error(), ctx, "save_user")
			output <- ResultUseCase{Error: saveResult.Error}
			return
		}

		output <- ResultUseCase{Error: nil}
	}()

	return output
}
