package usecase

import "bitbucket.org/Amartha/go-single-ddd/modules/user/model"

// ResultUseCase - structure for use case result
type ResultUseCase struct {
	Result interface{}
	Error  error
}

// UserUseCase - interface for user use case
type UserUseCase interface {
	GetUsers() <-chan ResultUseCase
	GetUserByID(id int) <-chan ResultUseCase
	SaveUser(user model.User) <-chan ResultUseCase
}
