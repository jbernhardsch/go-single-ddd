package presenter

import (
	"errors"
	"net/http"

	"bitbucket.org/Amartha/go-single-ddd/helper"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/model"
	"bitbucket.org/Amartha/go-single-ddd/modules/user/usecase"
	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"strconv"
)

// HTTPUserHandler - structure for user http handler
type HTTPUserHandler struct {
	userUseCase usecase.UserUseCase
}

// NewHTTPHandler - for initializing http user model
func NewHTTPHandler(uUseCase usecase.UserUseCase) *HTTPUserHandler {
	return &HTTPUserHandler{userUseCase: uUseCase}
}

// Mount - function for mounting routes
func (h *HTTPUserHandler) Mount(group *echo.Group) {
	group.GET("", h.GetUserList)
	group.GET("/:id", h.GetDetailUser)
	group.POST("", h.AddNewUser)
}

// GetUserList - function for getting list of user
func (h *HTTPUserHandler) GetUserList(c echo.Context) error {
	ctx := "UserPresenter-GetUserList"

	usersResult := <-h.userUseCase.GetUsers()
	if usersResult.Error != nil {
		helper.Log(log.ErrorLevel, usersResult.Error.Error(), ctx, "get_users")
		return echo.NewHTTPError(http.StatusInternalServerError, usersResult.Error.Error())
	}

	users, ok := usersResult.Result.([]model.User)
	if !ok {
		err := errors.New("result is not list of user")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "assert_users")
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, users)
}

// GetDetailUser - function for getting detail user
func (h *HTTPUserHandler) GetDetailUser(c echo.Context) error {
	ctx := "UserPresenter-GetDetailUser"

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		err = errors.New("invalid user id")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "validate_id")
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	userResult := <-h.userUseCase.GetUserByID(id)
	if userResult.Error != nil {
		helper.Log(log.ErrorLevel, userResult.Error.Error(), ctx, "get_user")
		return echo.NewHTTPError(http.StatusInternalServerError, userResult.Error.Error())
	}

	user, ok := userResult.Result.(model.User)
	if !ok {
		err := errors.New("result is not user")
		helper.Log(log.ErrorLevel, err.Error(), ctx, "assert_user")
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, user)
}

// AddNewUser - function for adding new user
func (h *HTTPUserHandler) AddNewUser(c echo.Context) error {
	ctx := "UserPresenter-AddNewUser"

	data := model.User{}
	err := c.Bind(&data)
	if err != nil {
		helper.Log(log.ErrorLevel, err.Error(), ctx, "bind_request_data")

		err = errors.New("request data is invalid")
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	saveResult := <-h.userUseCase.SaveUser(data)
	if saveResult.Error != nil {
		helper.Log(log.ErrorLevel, saveResult.Error.Error(), ctx, "save_user")
		return echo.NewHTTPError(http.StatusInternalServerError, saveResult.Error.Error())
	}

	response := struct {
		Message string `json:"message"`
	}{Message: "success to proceed data"}

	return c.JSON(http.StatusCreated, response)
}
