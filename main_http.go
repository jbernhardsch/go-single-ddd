package main

import (
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/Amartha/go-single-ddd/middleware"
	"github.com/labstack/echo"

	healthPresenter "bitbucket.org/Amartha/go-single-ddd/modules/health/presenter"
	userPresenter "bitbucket.org/Amartha/go-single-ddd/modules/user/presenter"
)

const DefaultPort = 8080

// HTTPServerMain - function for initiate http services
func (s *Service) HTTPServerMain() {
	e := echo.New()
	e.Use(middleware.ServerHeader, middleware.Logger)

	if os.Getenv("ENV") == "DEV" {
		e.Debug = true
	}

	// health endpoints
	healthHandler := healthPresenter.NewHTTPHandler(s.HealthUseCase)
	healthGroup := e.Group("/api/health")
	healthHandler.Mount(healthGroup)

	// user endpoints
	userHandler := userPresenter.NewHTTPHandler(s.UserUserCase)
	userGroup := e.Group("/api/user")
	userHandler.Mount(userGroup)

	// set REST port
	var port uint16
	if portEnv, ok := os.LookupEnv("PORT"); ok {
		portInt, err := strconv.Atoi(portEnv)
		if err != nil {
			port = DefaultPort
		} else {
			port = uint16(portInt)
		}
	} else {
		port = DefaultPort
	}

	listenerPort := fmt.Sprintf(":%d", port)
	e.Logger.Fatal(e.Start(listenerPort))
}
