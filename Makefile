.PHONY : all clean format cover coverage test single-ddd-linux single-ddd-osx

all: coverage

format:
	find . -name "*.go" -not -path "./vendor/*" -not -path ".git/*" | xargs gofmt -s -d -w

clean:
	[ -f single-ddd-osx ] && rm single-ddd-osx || true
	[ -f single-ddd-linux ] && rm single-ddd-linux || true
	[ -f coverage.txt ] && rm coverage.txt || true
	rm ./coverages/*.txt

single-ddd-osx: main.go
	GOOS=darwin GOARCH=amd64 go build -ldflags '-s -w' -o $@

single-ddd-linux: main.go
	GOOS=linux GOARCH=amd64 go build -ldflags '-s -w' -o $@

cover: coverage.txt coverage

coverage.txt: coverages/middleware.txt coverages/grpc-interceptor.txt coverages/config.txt coverages/user-query.txt
	gocovmerge $^ > $@

coverages/middleware.txt:  $(shell find ./middleware -type f)
	go test -race -short -coverprofile=$@ -covermode=atomic ./middleware

coverages/grpc-interceptor.txt:  $(shell find ./grpc/middleware -type f)
	go test -race -short -coverprofile=$@ -covermode=atomic ./grpc/middleware

coverages/config.txt:  $(shell find ./config -type f)
	go test -race -short -coverprofile=$@ -covermode=atomic ./config

coverages/user-query.txt:  $(shell find ./config -type f)
	go test -race -short -coverprofile=$@ -covermode=atomic ./modules/user/query

coverage:
	./codecov

test: ./helper ./middleware
	go test -race -short \
		 ./middleware \
		 ./grpc/middleware \
		 ./config \
		 ./modules/user/query
