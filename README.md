# Single DDD

[![codecov](https://codecov.io/bb/Amartha/go-single-ddd/branch/master/graph/badge.svg?token=HbBtGznZGq)](https://codecov.io/bb/Amartha/go-single-ddd)

This is scaffolding of single DDD design pattern using golang. This system serves http and grpc as the example.

This service is built with :heart: and Go.


## Requirements

- Golang version 1.8+
- [Glide](https://github.com/Masterminds/glide)
- other needed modules

 
## Installation 
 
 - To run the service you should install the required packages from glide package manager lock.
   ```
   glide install
   ```
 - Copy the `.env.dist.production` to `.env` in order to be loaded to the environment variables
 - Build the binary `Make single-ddd-osx` or `Make single-ddd-linux` depends on operating system where you run the service
 - Run `./single-ddd-osx` or `./single-ddd-linux`

 
## Running Tests and Coverage 
 
The software is designed to be able to run both on the machine on top of docker. You can see `Makefile` to see what
kind of targets supported.

 
### Integration Test
 
This test should be run with environment variable set.
 
```
make test
```
 
 
### Integration Test With Coverage
 
This test is to produce covarage report. We use `gocovmerge` to merge coverage reports from the packages. To run, use

```
make cover
```