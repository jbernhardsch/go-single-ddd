package main

import (
	"fmt"
	"os"
	"strconv"

	rpc "bitbucket.org/Amartha/go-single-ddd/grpc/servers"
	"bitbucket.org/Amartha/go-single-ddd/helper"
	healthPresenter "bitbucket.org/Amartha/go-single-ddd/modules/health/presenter"
	log "github.com/Sirupsen/logrus"
)

// GRPCDefaultPort default port for GRPC
const GRPCDefaultPort = 8082

// GRPCServerMain - function for initiate grpc services
func (s *Service) GRPCServerMain() {
	// set GRPC port
	port := GRPCDefaultPort
	portGRPC, ok := os.LookupEnv("PORT_GRPC")
	if ok {
		intPort, _ := strconv.Atoi(portGRPC)

		port = intPort
	}

	helper.Log(log.InfoLevel, fmt.Sprintf("GRPC server will be running on port %d", port), "grpc_main", "initiate_grpc")

	// include all GRPC presenters
	hGRPCHandler := healthPresenter.NewGRPCHandler(s.HealthUseCase)

	// initiate GRPC server
	grpcServer := rpc.NewGRPCServer(hGRPCHandler)

	err := grpcServer.Serve(uint(port))
	if err != nil {
		err = fmt.Errorf("error in Startup: %s", err.Error())
		helper.Log(log.ErrorLevel, err.Error(), "grpc_main", "serve_grpc")
		os.Exit(1)
	}
}
