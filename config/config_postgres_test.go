package config

import (
	"go/build"
	"testing"

	config "github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

// TestPostgresDBConnection - function for testing database connection
func TestPostgresDBConnection(t *testing.T) {
	envDir := build.Default.GOPATH + "/src/bitbucket.org/Amartha/go-single-ddd/"
	err := config.Load(envDir + ".env")
	if err != nil {
		assert.Error(t, err)
	}

	if testing.Short() {
		t.Skip("Skipping Integration Test on Short Mode")
	}

	t.Run("TestWritePostgresDBConnection", func(t *testing.T) {
		db := WritePostgresDB()

		err := db.Ping()

		assert.NoError(t, err)

	})

	t.Run("TestReadPostgresDBConnection", func(t *testing.T) {
		db := ReadPostgresDB()

		err := db.Ping()

		assert.NoError(t, err)

	})
}
