package middleware

import (
	"github.com/labstack/echo"
)

// ServerHeader - function for serving custom header
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "amartha-single-ddd/1.0 (AArch64)")
		return next(c)
	}
}
