package main

import (
	healthQuery "bitbucket.org/Amartha/go-single-ddd/modules/health/query"
	healthUseCase "bitbucket.org/Amartha/go-single-ddd/modules/health/usecase"

	userQuery "bitbucket.org/Amartha/go-single-ddd/modules/user/query"
	userRepo "bitbucket.org/Amartha/go-single-ddd/modules/user/repository"
	userUseCase "bitbucket.org/Amartha/go-single-ddd/modules/user/usecase"

	"bitbucket.org/Amartha/go-single-ddd/config"
)

// Service - structure for main handler
type Service struct {
	HealthUseCase healthUseCase.HealthUseCase
	UserUserCase  userUseCase.UserUseCase
}

// MakeHandler - function for initiate handler
func MakeHandler() *Service {
	// initiate database connection
	readDB := config.ReadPostgresDB()
	// writeDB := config.WritePostgresDB()

	hQuery := healthQuery.NewHealthQueryImpl("I'm fine!!")
	hUseCase := healthUseCase.NewHealthUseCase(hQuery)

	uQuery := userQuery.NewUserQueryPostgres(readDB)
	uRepo := userRepo.NewUserRepoPostgres(readDB)
	uUseCase := userUseCase.NewUserUseCase(uQuery, uRepo)

	return &Service{
		HealthUseCase: hUseCase,
		UserUserCase:  uUseCase,
	}
}
