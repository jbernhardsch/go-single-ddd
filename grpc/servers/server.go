package servers

import (
	"fmt"
	"net"

	"bitbucket.org/Amartha/go-single-ddd/grpc/middleware"
	"google.golang.org/grpc"

	healthPresenter "bitbucket.org/Amartha/go-single-ddd/modules/health/presenter"
	healthPB "bitbucket.org/Amartha/go-single-ddd/protogo/health"
)

// Server data structure
type Server struct {
	healthGRPCHandler *healthPresenter.GRPCHandler
}

// NewGRPCServer function for creating GRPC server
func NewGRPCServer(healthGrpcHandler *healthPresenter.GRPCHandler) *Server {
	return &Server{
		healthGRPCHandler: healthGrpcHandler,
	}
}

// Serve insecure server/ no server side encryption
func (s *Server) Serve(port uint) error {
	address := fmt.Sprintf(":%d", port)

	l, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	server := grpc.NewServer(
		//Unary interceptor
		grpc.UnaryInterceptor(middleware.Auth),
	)

	//Register all sub server here
	healthPB.RegisterPingPongServiceServer(server, s.healthGRPCHandler)
	//end register server

	err = server.Serve(l)

	if err != nil {
		return err
	}

	return nil
}
